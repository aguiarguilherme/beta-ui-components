const theme = require('@hexagonpro/tailwind-theme');

module.exports = {
  ...theme,
  purge: ['./src/**/*.{js,jsx,ts,tsx}'],
  presets: [require('./preset.js')],
};
