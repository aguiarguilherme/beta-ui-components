import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { User } from './icons/User';

export function Avatar({ children, size, ...props }) {
  return (
    <div
      className={`${classNames(
        'bg-brand-primary rounded-full flex items-center justify-center',
        {
          'h-8 w-8': size === 'sm',
          'h-12 w-12': size === 'md',
          'h-16 w-16': size === 'lg',
        }
      )}`}
      {...props}
    >
      <User
        className={classNames('text-brand-background', {
          'h-4 w-4': size === 'sm',
          'h-6 w-6': size === 'md',
          'h-8 w-8': size === 'lg',
        })}
      />
    </div>
  );
}

Avatar.propTypes = {
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
};

Avatar.defaultProps = {
  size: 'md',
};
