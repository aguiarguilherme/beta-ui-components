module.exports = {
  theme: {
    colors: {
      brand: {
        DEFAULT: '#0093B2',
        primary: '#0093B2',
        secondary: '#02C6F2',
        'tertiary-light': '#BFF0FB',
        'tertiary-dark': '#47555A',
        disabled: '#c4c4c4',
        success: '#55AE69',
        warning: '#ACAE55',
        danger: '#CE5858',
        'light-gray': '#ECECEC',
        gray: '#CCCCCC',
        'dark-gray': '#8F8F8F',
        'dark-gray-dark': '#484848',
        text: '#333333',
        'text-dark': '#FFFFFF',
        'secondary-text': '#6B6B6B',
        'secondary-text-dark': '#E0E0E0',
        background: '#FFFFFF',
        'background-dark': '#1F2224',
      },
    },
  },
};
